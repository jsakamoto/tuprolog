%Esempio 1:

demo_1(X,Y,R) :- load_library('TestLibrary','C:\', 'C:\Users\MyLibrary.jar']), 
  R is sum(X, Y).

%Esempio 2:

demo_2(Res) :- java_object('Counter', [], MyCounter, ['C:\Users\A.jar']),
  java_object('Counter[]', [10], ArrayCounters, ['C:\Users\A.jar']),
  MyCounter <- inc, java_array_set(ArrayCounters, 0, MyCounter),
  java_array_get(ArrayCounters, 0, C),
  C <- getValue returns Res.

%Esempio 5:

java_object('Counter', [], Obj,['C:\Users\A.jar']), 
  Obj <- inc,
  Obj <- inc, 
  Obj <- getValue returns Val.

%Esempio 6:

class(['C:\ ', 'C:\Users\A.jar'], 'TestStaticClass') <- echo('Message') returns Val.

%Esempio 7:
class(['C:\', 'C:\Users\A.jar'], 'TestStaticClass').'id' <- get(Value) returns Val.

%Esempio 8:
java_object('Counter', [], MyCounter, ['C:\Users\A.jar']),  
java_object('Counter[]', [10], ArrayCounters, ['C:\Users\A.jar']),
java_array_length(ArrayCounters, Size).

%Esempio 9:
demo(List) :- set_classpath(['C:\', 'C:\Users\A.jar']), 
  get_classpath(List).

%Esempio 10:

demo(Obj) :- java_object('Counter', [], Obj, ['C:\Users\A.jar']),
   Obj <- inc, 
   Obj <- inc, 
   register(Obj).

demo2(Obj, Val) :- Obj <- inc, Obj <- getValue returns Val.

%Esempio 12:

demo(X, Y, Res) :- load_library('TestLibraryCS.TestLibraryCS, TestLibraryCS',['C:\Users\TestLibraryCS.dll']), 
	Res is sum(X, Y).



